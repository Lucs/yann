#include <stdio.h>

void ft_is_negative(int num)
{
    if (num >=0)
    {
        putchar('P');
    }
    else 
    {
        putchar('N');
    }
}

int main()
{
    int num;
    printf("Enter a number: ");
    scanf("%d", &num);

    ft_is_negative(num);
}