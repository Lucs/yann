#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		main()
{
	int arr[] = {9, 20, 4, 5, 12, 1, 8, 200};
	int size = 8;
	int i = 0;
	
	printf("before :");
	while (i < size)
	{
		printf("%d ", arr[i]);
		i++;
	}
	printf("\n");
	
	ft_sort_integer_table(arr, size);
	
	printf("after :");
	i = 0;
	while (i < size)
	{
		printf("%d ", arr[i]);
		i++;
	}
	printf("\n");
	
	return (0);
}