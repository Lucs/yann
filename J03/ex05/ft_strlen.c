#include <unistd.h>
#include <stdio.h>
#include "main.h"

int	ft_strlen(char *str)
{
	int size = 0;
	while (*str != '\0')
	{
		size++;
		str++;
	}
	return size;
}
