#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		ft_atoi(char *str)
{
	int num = 0;
	int bob = 1;					//ici bob va s'occuper des nombre négatif
	
	while (*str == ' ')
	{
		str++;
	}
	if (*str == '-')
	{
		bob = -1;
		str++;
	}
	else if (*str == '+')
	{
		str++;
	}
	
	while (*str >= '0' && *str <= '9')
	{
		num = num * 10 + (*str - '0');
		str++;
		if (*str == '\0')
			break;
	}
	num *= bob;
	return (num);
}