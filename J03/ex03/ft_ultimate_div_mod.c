#include <stdio.h>
#include <unistd.h>
#include "main.h"

void	ft_ultimate_div_mod(int *a, int *b) 	// result in a and mod in b
{
	int x = *a;
	*a = x / *b;
	*b = x % *b;
}
