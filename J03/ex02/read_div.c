#include <unistd.h>
#include <stdio.h>
#include "read_div.h"

int	main()
{
	int a;
	int b;
	int div;
	int mod;

	printf("entrer deux nombre entier : \n");
	scanf("%d%d", &a, &b);

	ft_div_mod(a, b, &div, &mod);

	printf("Le quotient de %d par %d est %d\n", a, b, div);
	printf("Le reste de la division de %d par %d est %d\n", a, b, mod);

	return (0);
}

