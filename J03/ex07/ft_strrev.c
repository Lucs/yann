#include <unistd.h>
#include <stdio.h>
#include "main.h"

int	ft_strlen(char *str)
{
	int size = 0;
	while (*str != '\0')
	{
		size++;
		str++;
	}
	return size;
}


char	*ft_strrev(char *str)
{
	int i = 0;
	int j = ft_strlen(str) - 1;
	while (i < j)
	{
		char bob = str[i];					// bob est une varaible temporaire c'est un peu une sorte de fourre tout 
		str[i] = str[j];
		str[j] = bob;
		i++;
		j--;
	}
	return (str);
}
	