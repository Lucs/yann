#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		main()
{
	int a = 1;
	int b = 2;
	
	ft_swap(&a, &b);
	printf( "a==%d, b==%d\n", a, b);
	
	return (0);
}