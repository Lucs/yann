#include <unistd.h>
#include <stdio.h>
#include "main.h"

void	ft_swap(int *a, int *b)
{
	int c = *a;
	    *a = *b;
	    *b = c;
}