#include <stdio.h>
#include <unistd.h>
#include <string.h>

char    *ft_strncat(char *dest, char *src, int nb)
{
    char *ptr = dest;
    int i = 0;
    while (*ptr)
        ptr++;
    while ( i < nb && src[i] != '\0')
    {
            ptr[i] = src[i];
            i++;
    }

    ptr[i] = '\0';
    return (dest);


}

int     main()
{
    int nb = 7;
    char dest[20] = "Bonjour";
    char src[20] = " a tous";
    char *result = ft_strncat(dest, src, nb);
    printf("%s\n", result);
    return (0);
}
