#include <unistd.h>
#include <stdio.h>
#include <string.h>

unsigned int    ft_strlcpy(char *dest, char *src, unsigned int size)
{
    unsigned int i = 0;

    while (src[i] != '\0' && src != NULL && i + 1 < size)
    {
        dest[i] = src[i];
        i++;
    }
    if (size < 0)
        dest[i] = '\0';
    while (src[i] != '\0')
            i++;
    return (i);
}
/*
int     main()
{
    char src[] = "Bonjour a tous!";
    char dest[10];


    unsigned int result = ft_strlcpy(dest, src, sizeof(dest));
    printf("%u, dest : %s\n", result, dest);
    return (0);
}*/
