#include <unistd.h>
#include <string.h>
#include <stdio.h>



char	*ft_strcpy(char *dest, char *src)
{
	if ( dest == NULL || src == NULL)
		return (NULL);
	
	int i = 0;
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

/*int main()
{
    char str1[] = "belle bite";
    char str2[];

    
    ft_strcpy(str2, str1);

    printf("str1 : %s\n", str1);
    printf("str2 : %s\n", str2);

    return 0;
}*/
