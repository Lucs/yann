#include <unistd.h>
#include <stdio.h>
#include <string.h>

unsigned int    ft_strlcat(char *dest, char *src, unsigned int size)
{
    unsigned int i = 0;
    unsigned int j = 0;
    unsigned int len_dest = 0;                                  // len pour lenght
    unsigned int len_src = 0;

    while (dest[len_dest] != '\0' && len_dest < size)
        len_dest++;

    while (src[len_src] != '\0')
        len_src++;

    if (len_dest == size)
        return len_dest + len_src;


    i = len_dest;
    while (i < size - 1 && src[j] != '\0')
    {
        dest[i] = src[j];
        i++;
        j++;
    }
    dest[i] = '\0';

    return len_dest + len_src;
}
/*
int     main()
{
    char str1[30] = "Bonjour";
    char str2[] = " a tous !";
    unsigned int result = ft_strlcat(str1, str2, sizeof(str1));
    printf("resultat %u\n", result);
    printf("fusion : %s\n", str1);
    return (0);
}*/
