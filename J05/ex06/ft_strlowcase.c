#include <unistd.h>
#include <stdio.h>
#include <string.h>

char    *ft_strupcase(char *str)
{
    int i = 0;
    while (str[i] != '\0')
    {
        if (str[i] >= 'A' && str[i] <= 'Z')
            str[i] = str[i] + ('a' - 'A');
        i++;
    }
    return (str);


}
/*
int     main()
{
    char str[] = "SALUT MA COUILLASSE!";
    printf("%s\n", ft_strupcase(str));
    return (0);

}*/
