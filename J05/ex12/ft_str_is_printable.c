#include <unistd.h>
#include <stdio.h>
#include <string.h>

int     ft_str_is_printable(char *str)
{
    if (str[0] == '\0')
        return (1);

    int i = 0;
    while (str[i] != '\0') {
        if (str[i] < 32 || str[i] > 126)                //l'astuce c'est de regarder une table ascii :)
            return (0);
        i++;
    }
    return (1);

}
/*
int     main()
{
    char str[] = "";

    if (ft_str_is_printable(str))
        printf("1\n");
    else
        printf("0\n");
}*/
