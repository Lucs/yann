#include <unistd.h>
#include <stdio.h>
#include <string.h>

int   CtoI(char c, char *base)
{
  int i;
  i = 0;

  while (base[i] != '\0')
  {
    if (c == base[i])
      return i;
    i++;
  }
  return (-1);
}

int   ft_atoi_base(char *str, char *base)
{
  int base_len = 0;

  while (base[base_len] != '\0')
    base_len++;

  int result = 0;
  int sign = 1;

  if (*str == '-')
  {
    sign = -1;
    str++;
  }

  while (*str != '\0')
  {
    int value = CtoI(*str, base);
    if (value ==  -1)
      return (0);

    result = result * base_len + value;
    str++;
  }
  return (result * sign);
}

int   main()
{
  char *str = "42";
  char *base = "01";

  int i = ft_atoi_base(str, base);
  printf("%d\n", i);
  return (0);               
}
