#include <unistd.h>
#include <string.h>
#include <stdio.h>

char	*ft_strstr(char *str, char *to_find)
{
	if (str == NULL || to_find == NULL)
		return (NULL);
	
	int i = 0;
	while (str[i])
	{
		int j = 0;
		while (to_find[j] && str[i+j] == to_find[j])
			j++;
		
		if (to_find[j] == '\0')
			return (&str[i]);
		i++;
	}
	return (NULL);
	

}

int main()
{
    char str1[] = "belle bite";
    char str2[] = "belle";
    char str3[] = "petit luc";

    printf("str1 : %s\n", str1);
    printf("str2 : %s\n", str2);
    printf("str3 : %s\n", str3);

    char *result1 = ft_strstr(str1, str2);
    char *result2 = ft_strstr(str1, str3);

    if (result1) {
        printf("'%s' trouver '%s' a position %ld\n", str2, str1, result1 - str1);
    } else {
        printf("'%s' pas de correspondance '%s'\n", str2, str1);
    }

    if (result2) {
        printf("'%s' trouver '%s' a position %ld\n", str3, str1, result2 - str1);
    } else {
        printf("'%s' pas de correspondance '%s'\n", str3, str1);
    }

    return 0;
}
	
		
	