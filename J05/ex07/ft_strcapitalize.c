#include <unistd.h>
#include <stdio.h>
#include <string.h>

int     ft_isalpha(int c) {
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
        return (1);
    }
    return (0);
}

char    *ft_strcapitalize(char *str)
{
    int i = 0;

    while (str[i] != '\0')
    {
        if (i == 0 || !ft_isalpha(str[i - 1]))
        {
            if ( str[i] >= 'a' && str[i] <= 'z')
                str[i] = str[i] - ('a' - 'A');
        }
        else {
            if (str[i] >= 'A' && str[i] <= 'Z') {
                str[i] = str[i] - ('a' - 'A');
            }
        }
        i++;
    }
    return (str);


}
/*
int     main()
{
    char str[] = "je 2suis @jean-marie 51bigard ma +couillasse !";
    printf("%s\n", ft_strcapitalize(str));
    return (0);
}*/
