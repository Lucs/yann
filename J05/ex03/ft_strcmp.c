#include <unistd.h>
#include <string.h>
#include <stdio.h>

int		ft_strcmp(char *s1, char *s2)
{
	while (*s1 != '\0' || *s2 != '\0')
	{
		if (*s1 != *s2)
			return (*s1 - *s2);

		s1++;
		s2++;
	}
	return (0);


}


/*int main() {
    char str1[] = "Hello";
    char str2[] = "World";
    char str3[] = "Hello";
    char str4[] = "HeLlo";

    printf("str1 : %s\n", str1);
    printf("str2 : %s\n", str2);
    printf("str3 : %s\n", str3);
    printf("str4 : %s\n", str4);

    printf("strcmp(str1, str2) = %d\n", ft_strcmp(str1, str2));
    printf("strcmp(str1, str3) = %d\n", ft_strcmp(str1, str3));
    printf("strcmp(str1, str4) = %d\n", ft_strcmp(str1, str4));
    printf("strcmp(str2, str1) = %d\n", ft_strcmp(str2, str1));
    printf("strcmp(str3, str4) = %d\n", ft_strcmp(str3, str4));

    return 0;
}*/
