#include <unistd.h>
#include <stdio.h>
#include <string.h>

int ft_strlen(char *str)
{
    int len = 0;
    while (*str++) len++;
      return len;
}


void    ft_putnbr_base(int nbr, char *base)
{
  int base_len = ft_strlen(base);

  if (base_len <= 1)
    return;

  if (nbr < 0)
  {
    write(1, "-", 1);
    nbr = -nbr;
  }

  char buffer[32];
  int i = 0;
  while (nbr != 0)
  {
    buffer[i++] = base[nbr % base_len];
    nbr /= base_len;
  }
  if (i == 0)
  {
    buffer[i++] = '0';
    buffer[i] = '\0';
  }
  int j = 0;
  while (j < i/2)
  {
    char tmp = buffer[j];
    buffer[j] = buffer[i-j-1];
    buffer[i-j-1] = tmp;
    j++;
  }
  write (1, buffer, i);

}

int   main()
{
  int nbr = 42;
  char *base = "0123456789ABCDEF";

  ft_putnbr_base(nbr, base);
  printf("\n");
  return (0);
}
