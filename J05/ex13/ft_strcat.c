#include <unistd.h>
#include <stdio.h>
#include <string.h>

char    *ft_strcat(char *dest, char *src)
{
    char *ptr = dest;
    while (*ptr)
        ptr++;
    while (*src)
        *ptr++ = *src++;
    *ptr = '\0';
    return (dest);

}

int     main()
{
    char dest[20] = "bonjour";
    char src[20] = " a tous!";

    char *result = ft_strcat(dest, src);
    printf("%s\n", result);
    return (0);
}
