#include <unistd.h>
#include <stdio.h>
#include <string.h>

int   ft_putchar(char c)
{
  write(1, &c, 1);

}

void  ft_putstr_non_printable(char *str)
{
  int i =0;

  while (str[i] != '\0')
  {
    if (str[i] >= 32 && str[i] <= 126)
    {
      ft_putchar(str[i]);
    }
    else
    {
      ft_putchar('\\');
      ft_putchar("0123456789abcdef"[str[i] / 16]);
      ft_putchar("0123456789abcdef"[str[i] % 16]);
    }
    i++;
  }
}
/*
int   main()
{
  char str[] = "coucou \ntu vas bien";

  ft_putstr_non_printable(str);
  ft_putchar('\n');

  return(0);
}*/ 

