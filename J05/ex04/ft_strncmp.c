#include <unistd.h>
#include <stdio.h>
#include <string.h>

int     ft_strncmp(char *s1, char *s2, unsigned int n)
{
  unsigned int i = 0;

  while (i < n && s1[i] != '\0' && s2[i] != '\0') {
    if (s1[i] != s2[i])
      return (s1[i] - s2[i]);
  }
  i++;

  if (i == n)
    return (0);
  else
    return (s1[i] - s2[i]);
}
/*
int   main()
{
  char str1[] = "hello";
  char str2[] = "bonjour";

  int result = ft_strncmp(str1, str2, 5);

  if (result == 0)
      printf("les deux chaine sont égale\n" );
  else if (result < 0)
      printf("la premiere chaine est plus petite que la seconde.\n");
  else
      printf("la premiere chaine est plus grande que la seconde.\n");

  return (0);

}*/
