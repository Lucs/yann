#include <unistd.h>
#include <string.h>
#include <stdio.h>

char	*ft_strncpy(char *dest, char *str, unsigned int n)
{
	if (str == NULL || dest == NULL)
		return (NULL);
	
	unsigned int i = 0;
	
	while (i < n && str[i] != '\0')
	{
		dest [i] = str[i];
		i++;
	}
	while ( i < n)
	{
		dest[i] = '\0';
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

/*int		main()
{
	char str1[20];
	char str2[] = "belle bite";
	
	ft_strncpy(str1, str2, 5);
	
	printf("str1: %s\n", str1);
	printf("str1: %s\n", str2);
}*/
