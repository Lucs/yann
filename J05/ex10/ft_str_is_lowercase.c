#include <unistd.h>
#include <stdio.h>
#include <string.h>

int     ft_str_is_lowercase(char *str)
{
    if (str[0] == '\0')
        return (1);

    int i =0;
    while (str[i] != '\0') {
        if (str[i] < 'a' || str[i] > 'z')
            return (0);
        i++;
    }
    return (1);
}
/*
int     main()
{
    char str[] = "";

    if(ft_str_is_lowercase(str))
        printf("1\n");
    else
        printf("0\n");

    return (0);
}*/
