#include <unistd.h>
#include <stdio.h>
#include <string.h>

int     ft_str_is_alpha(char *str)
{
    int i = 0;
    while (str[i] != '\0') {
        if (!(str[i] >= 'A' && str[i] <= 'Z') && !(str[i] >= 'a' && str[i] <= 'z' || str[i] == ' '))
            return (0);
        i++;
    }
    return (1);
}

/*
int     main()
{
    char str[] = "bonjour charle 4";



    printf("%x\n",ft_str_is_alpha(str));


    return (0);
}*/
