#include <unistd.h>
#include <stdio.h>
#include <string.h>

void	ft_print_hex(unsigned char c)
{
	char *hex = "0123456789abcdef";

	write(1, &hex[c / 16], 1);
	write(1, &hex[c % 16], 1);
}

void	ft_print_memory(const void *addr, size_t size)
{
	size_t	i = 0;
	size_t	j;
	char	buffer[17];

	while (i < size)
	{
		ft_print_hex(((unsigned char *)addr)[i]);
		write(1, " ", 1);
		j = 0;
		while (j < 16 && i + j < size)
		{
			if (((unsigned char *)addr)[i + j] >= 32 &&
				((unsigned char *)addr)[i + j] <= 126)
				write(1, &((unsigned char *)addr)[i + j], 1);
			else
				write(1, ".", 1);
			j++;
		}
		write(1, "\n", 1);
		i += 16;
	}
}

int   main()
{
  char str[] = "Bonjour a tous";
   printf("%p\n", (void *)str);
  ft_print_memory(str, strlen(str));
  return (0);
}
