#include <unistd.h>
#include <stdio.h>
#include <string.h>

int   ft_putchar(char c)
{                                   // fonction a chier
                                    // tous est a chier ICI
  write(1, &c, 1);
  return (0);
}

void  ft_print_hexa_addr(void *addr)
{
    long int address = (long int)addr;
    char *hexa = "0123456789abcdef";
    int i;

    ft_putchar('0');
    ft_putchar('x');
    for (i = 14; i >= 0; i -= 2)
        ft_putchar(hexa[(address >> i * 4) & 0xf]);
    ft_putchar(hexa[(address << 4) & 0xf]);
    ft_putchar('\n');
}


int   ft_is_printable(unsigned char c)
{
  if (c >= 32 && c <= 126)
    return (1);
  return(0);
}

void  *ft_print_memory(void *addr, unsigned int size)
{
    ft_print_hexa_addr(addr);
    unsigned char *ptr = (unsigned char *)addr;
    unsigned int i = 0;
    unsigned int j;


  while (i < size)
  {
    ft_print_hexa_addr(&ptr[i]);
    ft_putchar(' ');
    j = 0;

    while (j < 16 && i + j < size)
    {
      if(ft_is_printable(ptr[i + j]))
        ft_putchar(ptr[i + j]);
      else
        ft_putchar('.');
      j++;
    }
    ft_putchar('\n');
    i += 16;
  }
  return (addr);
}

int   main()
{
  char str[] = "Bonjour a tous";
   printf("%p\n", (void *)str);
  ft_print_memory(str, sizeof(str));
  printf("\n");
  return (0);
}
