#include <stdio.h>

void ft_print_alphabet()
{
    char lettre = 'a';
    while (lettre <= 'z')           
    {
        putchar(lettre);
        lettre++;
    }
    
}

int main()
{
    ft_print_alphabet();
    return 0;
}



