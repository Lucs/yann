#include <unistd.h>
#include <stdio.h>
#include <string.h>

int     ft_putchar(char c);
int     ft_strncmp(char *s1, char *s2, unsigned int n);


int main(int argc, char *argv[])
{
    int i;
    int e;
    int n;

    n = argc;
    i = 1;

    while (i < n)
    {
        e = i + 1;
        while (e < n)
        {
            if (ft_strncmp(argv[i], argv[e], 1) > 0)
            {
                char *temp = argv[i];
                argv[i] = argv[e];
                argv[e] = temp;
            }
            e++;
            
        }
        i++;
    }
    i = 1;
    while (i < n)
    {
        e = 0;
        while (argv[i][e] != '\0')
        {
            ft_putchar(argv[i][e]);
            e++;
        }
        ft_putchar('\n');
        i++;
    }


    return 0;
}
