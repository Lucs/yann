#include <unistd.h>
#include <stdio.h>
#include <string.h>

int   ft_putchar(char c);

int   main(int argc, char *argv[])
{
  char *name = argv[0];

  while (*name != '\0')
  {
    ft_putchar(*name);
    name++;
  }
  ft_putchar ('\n');

  return (0);
}

