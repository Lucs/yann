#include <unistd.h>
#include <stdio.h>
#include <string.h>

int   ft_putchar(char c);

int main(int argc, char *argv[])
{
  int i;
  int e;
  i = argc - 1;
  while (i >= 1)
  {
    e = 0;
    while (argv[i][e] != '\0')
	{
		ft_putchar(argv[i][e]);
		e++;
	}
	ft_putchar('\n');
	i--;
  }
	

  return (0);
}
