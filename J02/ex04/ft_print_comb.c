#include <unistd.h>
#include <stdio.h>

void	ft_print_comb(void)
{
	int a = 0;
	int b = 1;
	int c = 2;

	while (a <=7)
	{
		while (b <= 8)
		{
			while (c <= 9)
			{
				printf("%d%d%d", a, b, c);
				if (a !=7 || b !=9 || c != 9)
				{
					printf(", ");
				}
				c++;
			}
			b++;
			c = b +1;
		}
		a++;
		b = a + 1;
		c = b + 1;
		
	}

	printf("\n");
}
	
