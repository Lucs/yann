#include <unistd.h>
#include "read.h"

void ft_print_number(void)
{
	int i = 0;
	while (i <= 9)
	{
		ft_putchar(i + '0');
		i++;
	}
	ft_putchar('\n');
}
int main()
{
	ft_print_number();
	return (0);
}

