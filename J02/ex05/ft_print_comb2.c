#include <unistd.h>
#include <stdio.h>

int	ft_putchar(char c)
{
	write(1, &c, 1);
	return (0);
}




void	ft_print_comb2(void)
{
	int i, j;
	char a, b;

	i = '0';
	while (i <= '8')
	{
		j = i + 1;
		while (j <= '9')
		{
			a = i;
			b = j;
			ft_putchar(a);
			ft_putchar(b);

			if(!(a == '8' && b == '9'))
			{
				ft_putchar(',');
				ft_putchar(' ');
			}
			j++;
		}
		i++;
	}
	ft_putchar('\n');
}
