#include <unistd.h>
#include "read.h"

void ft_print_reverse_alphabet(void)
{
	char lettre = 'z';
	while (lettre >= 'a')
	{
		ft_putchar(lettre);
		lettre--;
	}
}

