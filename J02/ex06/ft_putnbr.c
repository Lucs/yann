#include <unistd.h>
#include "main.h"

void ft_putchar(char c);

void	ft_putnbr(int nb)
{
	if (nb < 0)				// on teste la négation
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb >= 10)				// on regarde si le nombre a plus de 2 chiffres
	{
		ft_putnbr(nb / 10);
	}
	ft_putchar(nb % 10 + '0');	// unité modulo into +'0' go ascii print avec putchar
}	

