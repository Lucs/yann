#include <unistd.h>
#include <stdio.h>
#include "main.h"

int	ft_putchar(char c)
{
	write(1, &c, 1);
	return (0);
}

void	ft_bob (int comb[], int cbi, int debut, int n)
{
	if (cbi == n)
	{
		int i = 0;
		while ( i < n)
		{
			ft_putchar(comb[i] + '0');
			i++;
		}

		if (comb[0] != 10 - n)
		{	
			ft_putchar(',');
			ft_putchar(' ');
		}
	return;	
	}

	int i = debut;
	while (i <= 10 - n + cbi)
	{
		comb[cbi] = i;
		ft_bob(comb, cbi + 1, i + 1, n);
		i++;
	}
}




void	ft_print_combn(int n)
{
	int comb[n];
	int i = 0;
	while (i < n)
	{
		comb[i] = i;
		i++;
	}

	int k = 1;
	while (k <= n)
	{
		ft_bob(comb, 0, 0, k);
		k++;
	}
}
