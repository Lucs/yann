#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		colle(int x, int y)					// x largeur et y hauteur
{

	int i = 0;
	
	while (i < y)
	{
		int j = 0;
		while (j < x)
		{
			if (i == 0 || i == y - 1 || j == 0 || j == x - 1)
			{
				ft_putchar('#');
			}
			
			else
			{
				ft_putchar(' ');
			}
			
			j++;
		}
		ft_putchar('\n');
		i++;
	}
	return (0);
}