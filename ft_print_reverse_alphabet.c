#include <stdio.h>

void ft_print_reverse_alphabet(void)
{
    char lettre = 'z';
    while (lettre >= 'a')
    {
        putchar(lettre);
        lettre--;
    }

}
int main()
{
    ft_print_reverse_alphabet();
    return 0;
}