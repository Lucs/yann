#include <stdio.h>

void ft_print_number(int number)
{
    if (number > 9)
    {
        putchar('\n');
        return;
    }
    putchar(number + '0');
    ft_print_number(number + 1);
    
}

int main()
{
    ft_print_number(0);
    return 0;
}