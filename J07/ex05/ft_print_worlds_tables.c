#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

char **ft_split_whitespaces(char *str);

int     ft_putchar(char c)
{
    write (1, &c, 1);
    return (0);
}

void	ft_putstr(char *str)
{
	int i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}

}

void ft_print_worlds_tables(char **tab)
{
    int i;

    if (tab == NULL)
    {
        ft_putchar("Erreur tableau vide \n");
        return;
    }
    i = 0;
    while (tab[i] != NULL)
    {
        ft_putstr(tab[i]);
        ft_putchar('\n');
        i++;
    }
}