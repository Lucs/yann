#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int     is_sep(char c, char *charset)
{
    while (*charset)
    {
        if (c == * charset)
            return (1);
        charset++;
    }
}

int     count_worlds(char *str, char *charset)
{
    int count;
    count = 0;
    
    while (*str)
    {
        while (*str && is_sep(*str, charset))
            str++;
        if (*str && !is_sep(*str, charset))
        {
            count++;
            while (*str && !is_sep(*str, charset))
                str++;
        }
    }
    return (count);
}

char    **ft_split(char *str, char *charset)
{
     char **tab;
    int i;
    int j;
    int k;

    int count = count_worlds(str, charset);
    tab = (char **)malloc((count + 1) * sizeof(char *));
    if (!tab)
        return (NULL);

    i = 0;

    while (*str)
    {
        while (*str && is_sep(*str, charset))
            str++;
        if (*str && !is_sep(*str, charset))
        {
            j = 0;
            while (str[j] && !is_sep(str[j], charset))
                j++;
            tab[i] = (char*) malloc((j+1) * sizeof(char));
            if (!tab[i])
                return (NULL);
            k = 0;
            while (k < j)
            {
                tab[i][k] = str[k];
                k++;
            }
            tab[i][k] = '\0';
            i++;
            str += j;
        }
    }
    tab[i] = NULL;
    return (tab);
}

int     main(void)
{
    char str[] = "Bonjour a tous";      //  nique bien ta mere avec tes exo phillipe 
    char charset[] = "123456789";
    char **result = ft_split(str, charset);
    int i;

    if (result == NULL)
    {
        printf("Vous devez faire erreur \n");
        return (1);
    }
    i = 0;
    while (result[i] != NULL)
    {
        printf("%s\n", result[i]);
        i++;
    }
    free (result);
    return (0);
}