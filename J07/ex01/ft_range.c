#include <unistd.h>
#include <stdlib.h>
#include <string.h>

void	ft_putnbr(int nb);
int     ft_putchar(char c);

int     *ft_range(int min, int max)
{
    int *range;
    int i;
    int size;

    if (min >= max)
        return (NULL);
    
    size  = max - min - 1;
    range = (int*)malloc(size * sizeof(int));

    if (range == NULL)
        return (NULL);
    
    i = 0;
    while (i < size)
    {
        range[i] = min + i + 1;
        i++;
    }
    
    return (range);
    free (range);
}


