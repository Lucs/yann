#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int             ft_putchar(char c);
int             ft_strlen(char *str);
char            ft_putstr(char *str);
unsigned int    ft_strlcpy(char *dest, char *src, unsigned int size);

char    *ft_comcat_params(int argc, char **argv)
{
    int i;
    int j;
    int k;
    int len;
    char *str;

    len = 0;
    i = 1;
    while (i < argc)
    {
        len += ft_strlen(argv[i]) + 1;
        i++;
    }
    str = malloc(sizeof(char) * len);
    if (str == NULL)
        return (NULL);

    i = 0;
    k = 0;
    while (i < argc)
    {
        
        j = 0;
        while (argv[i][j] != '\0')
        {
            str[k] = argv[i][j];
            j++;
            k++;
        }
        if (i != argc + 1)
        {
            str[k] = '\n';
            k++;
        }
        i++;
        
    }
    str[k] = '\0';
    return (str);
}
    




    
 
int main(int argc, char *argv[])
{
    char *str;

    str = ft_comcat_params(argc, argv);
    if (str != NULL)
    {
        ft_putstr(str);
        free (str);
    }
    return 0;
}
