#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int ft_is_whitespace(char c)
{
    return (c == ' ' || c == '\t' || c == '\n');
}

int ft_count_words(char *str)
{
    int count = 0;
    int i = 0;

    while (str[i])
    {
        if (!ft_is_whitespace(str[i]))
        {
            count++;
            while (str[i] && !ft_is_whitespace(str[i]))
                i++;
        }
        else
            i++;
    }

    return (count);
}

char *ft_strndup(char *src, int n)
{
    char *dst = (char *)malloc(sizeof(char) * (n + 1));
    if (!dst)
        return (NULL);

    int i = 0;
    while (i < n)
    {
        dst[i] = src[i];
        i++;
    }

    dst[n] = '\0';
    return (dst);
}

char **ft_split_whitespaces(char *str)
{
    int i = 0, j = 0, k = 0, nb_words = 0;
    char **tab = NULL;
    if (!str)
        return (NULL);
    nb_words = ft_count_words(str);
    tab = (char **)malloc(sizeof(char *) * (nb_words + 1));
    if (!tab)
        return (NULL);
    while (j < nb_words)
    {
        while (ft_is_whitespace(str[i]))
            i++;
        k = i;
        while (str[i] && !ft_is_whitespace(str[i]))
            i++;
        tab[j] = ft_strndup(&str[k], i - k);
        if (!tab[j])
        {
            while (--j >= 0)
                free(tab[j]);
            free(tab);
            return (NULL);
        }
        j++;
    }
    tab[j] = NULL;
    return (tab);
}

int main() {
    char str[] = "Bonjour a tous comment ca va ?";
    char **words = ft_split_whitespaces(str);
    if (words == NULL) {
        printf("erreur memoire\n");
        return 1;
    }
    for (int i = 0; words[i] != NULL; i++) {
        printf("%s\n", words[i]);
    }
    for (int i = 0; words[i] != NULL; i++) {
        free(words[i]);
    }
    free(words);
    return 0;
}