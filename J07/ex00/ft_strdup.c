#include <unistd.h>
#include <stdlib.h>
#include <string.h>


unsigned int    ft_strlcpy(char *dest, char *src, unsigned int size);
int             ft_strlen(char *str);
void            ft_putstr(char *str);

char    *ft_strdup(char *src)
{
    size_t len = ft_strlen(src) + 1;
    char *str = malloc(sizeof(*str) * len + 1);
    if (str == NULL)
        return (NULL);

    ft_strlcpy(str, src, len);
    str[len] = '\0';
    return (str);
    free (str);


}

