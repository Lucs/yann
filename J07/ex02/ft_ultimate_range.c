#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int     ft_putstr(char str);

int     ft_ultimate_range(int **range, int min, int max)
{
    int i;
    int size;
    int *len;

    i = 0;

    if (min >= max)
    {
        *range = NULL;
        return (0);
    }

    size = max - min;
    len = (int*)malloc(size * sizeof(int));

    if (len == NULL)
    {
        *range = NULL;
        return (0);
    }
    while (i < size)
    {
        len[i] = min + i + 1;

        i++;
    }
    *range = len;
    return (size);

    
}
int     main()
{
    int *str;
    int i;
    i = 0;
    ft_ultimate_range(&str, 2, 5);
    while (i < 3)
    {
        printf("%d\n",str[i]);
        i++;
    }
    free (str);
    return (0);
}

