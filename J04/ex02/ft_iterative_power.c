#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		ft_iterative_power(int nb, int power)
{
	int result = 1;
	while ( power > 0)
	{
		result *= nb;
		power--;
	}
	return result;
}