#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		main()
{
	int nombre;
	int resultat;
	
	printf("entrée nombre entier :");
	scanf("%d", &nombre);
	
	resultat = ft_sqrt(nombre);
	
	if (resultat == 0)
		printf("negatif ou pas entier \n");
	else
		printf("Racine carré de %d et %d\n", nombre, resultat);
	
	return (0);
}
	
	