#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		ft_sqrt(int nb)
{
	int racine = -1;
	int a = 0;
	
	while ( a <= nb)
	{
		if (a * a == nb){
			racine = a;
			break;
		}
		a++;
	}
	
	if (racine == -1)
		return (0);
	else
		return (racine);
}
	
	