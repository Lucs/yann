#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		ft_recursive_power(int nb, int power)
{
	if (power == 0)
		return (1);
	else if (power % 2 == 0)
	{
		int temp = ft_recursive_power(nb, power / 2);
		return (temp * temp);
	}
	else
		return nb * ft_recursive_power(nb, power - 1);
	
}
	