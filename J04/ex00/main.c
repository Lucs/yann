#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		main()
{
	int nb;
	printf("nombre :");
	scanf("%d", &nb);
	int result = ft_iterative_factorial(nb);
	printf("factoriel de %d est %d\n", nb, result);
	return (0);
}