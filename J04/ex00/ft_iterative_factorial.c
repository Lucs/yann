#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		ft_iterative_factorial(int nb)
{
	if (nb == 0)
	{
		return 1;
	}
	else
	{
		return nb * ft_iterative_factorial(nb - 1);
	}
}