#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		main()
{
	int nb;
	
	printf("entrer nombre : ");
	scanf("%d", &nb);
	
	if (ft_is_prime(nb))
	{
		printf("%d est un nombre premier \n", nb);
	}
	else
		printf("%d pas un nombre premier \n", nb);
	
	return (0);
}
