#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		ft_is_prime(int nb)
{
	int a = 2;
	
	if (nb <= 1)
		return (0);
	
	while (a <= nb/2)
	{
		if (nb % a == 0)
		{
			return (0);
		}
		a++;
	}
	
	return (1);
}

