#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		main()
{
	int nb;
	int next_prime;
	
	printf("nombre entier P :");
	scanf("%d", &nb);
	
	next_prime = ft_find_next_prime(nb);
	
	printf(" le nombre premier le plus proche de %d est %d\n", nb, next_prime);
	
	return (0);
}
	
	
