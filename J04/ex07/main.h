#ifndef MAIN_H
#define MAIN_H

int		ft_is_prime(int nb);
int		ft_find_next_prime(int nb);

#endif 