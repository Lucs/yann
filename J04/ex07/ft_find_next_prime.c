#include <unistd.h>
#include <stdio.h>
#include "main.h"

int		ft_is_prime(int nb);

int		ft_find_next_prime(int nb)
{
	if (ft_is_prime(nb))
		return (nb);
	
	int next = nb + 1;
	
	int bas = nb + 1;
	while (!ft_is_prime(bas))
		bas--;
	
	int haut = nb + 1;
	while (!ft_is_prime(haut))
		haut--;
	
	if (nb - bas < haut - nb)
		return (bas);
	else
		return (haut);
}
	
